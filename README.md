# Goals

1) WiPy control of addressable LED Strip
2) Local (AP or STA) web controller for addressable LED Strip
3) Cloud controller for ground device (reuse web controller)
