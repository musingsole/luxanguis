from machine import reset
from time import sleep
from urequests import get, post
from ws2812 import WS2812


class LuxAnguis:
    app_url = "https://app.occultronicprovisions.com/luxanguis"

    @classmethod
    def get_config(cls):
        config_url = cls.app_url + "/configuration"
        return get(config_url).json()

    @classmethod
    def write_config(cls, config):
        config_url = cls.app_url + "/configuration"
        resp = post(config_url, json=config)
        assert resp.status_code == 200
        return resp

    def __init__(self):
        self.config = self.get_config()
        if 'reset' in self.config and self.config['reset'] is True:
            self.config['reset'] = False
            self.write_config(self.config)
        self.ws2812 = WS2812(ledNumber=int(self.config['num_leds']), brightness=1)
        self.pose_cache = {}
        self.animation_cache = {}

    def get_poses(self):
        url = "{}/poses".format(self.app_url)
        return get(url).json()

    def get_pose(self, pose_name):
        if pose_name in self.pose_cache:
            return self.pose_cache[pose_name]

        url = "{}/poses/{}".format(self.app_url, pose_name)
        pose = get(url).json()
        self.pose_cache[pose_name] = pose
        return pose

    def show_pose(self, pose):
        led_states = pose['led_states']
        self.ws2812.brightness = int(pose['brightness'])
        self.ws2812.show(led_states)

    def get_and_show_pose(self, pose_name):
        pose = self.get_pose(pose_name)
        self.show_pose(pose)

    def get_animations(self):
        url = "{}/animations".format(self.app_url)
        return get(url).json()

    def get_animation(self, animation_name):
        if animation_name in self.animation_cache:
            return self.animation_cache[animation_name]

        url = "{}/animations/{}".format(self.app_url, animation_name)
        animation = get(url).json()
        self.animation_cache[animation_name] = animation
        return animation

    def execute_animation(self, animation_name):
        print("Executing {} animation".format(animation_name))
        animation = self.get_animation(animation_name)
        pose_names = list(set([pose_def['pose_name'] for pose_def in animation['poses']]))
        poses = {pn: self.get_pose(pn) for pn in pose_names}

        for pose_def in animation['poses']:
            pose_name = pose_def['pose_name']
            print("Striking {} pose".format(pose_name))
            pose = poses[pose_name]
            self.show_pose(pose)
            sleep(float(pose_def['duration']))

    def heartbeat(self):
        print("Taking heartbeat")
        self.config = self.get_config()
        self.ws2812.brightness = int(self.config.get('brightness', 100))

        if "reset" in self.config and self.config['reset'] is True:
            print("Received reset request")
            reset()

    def execute_queue(self):
        while True:
            for animation in self.config.get('queue', []):
                self.heartbeat()
                self.execute_animation(animation)

