from network import WLAN
from time import sleep


known_networks = {
    "noobiemcfoob": (WLAN.WPA2, "n00biemcfoob"),
    "freeside-members": (WLAN.WPA2, "terminus")}


print("Attempting to connect to known network")
wlan = WLAN(mode=WLAN.STA)
for visible_network in wlan.scan():
    try:
        auth = known_networks[visible_network.ssid]
        wlan.connect(visible_network.ssid, auth=auth)
        waits = 0
        while not wlan.isconnected():
            if waits > 10:
                raise Exception("Failed to connect to {}".format(visible_network.ssid))
            waits += 1
            sleep(1)
        print("Connected to {} with {}".format(visible_network.ssid, wlan.ifconfig()))
        break
    except Exception:
        print("Failed to connect to {}".format(visible_network.ssid))

