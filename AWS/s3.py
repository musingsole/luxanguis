import json
from io import BytesIO
import boto3


bucket = "musingsole"


def retrieve_s3(s3_path):
    print(f"Retrieving {s3_path}")
    s3b = boto3.resource("s3").Bucket(bucket)
    contents = BytesIO()
    s3b.download_fileobj(s3_path, contents)
    return contents


def list_s3(s3_path):
    print(f"Listing {s3_path}")
    s3b = boto3.resource("s3").Bucket(bucket)
    return s3b.objects.filter(Prefix=s3_path)


def retrieve_json(s3_path):
	content = retrieve_s3(s3_path)
	content.seek(0)
	return json.loads(content.read())


def write_s3(s3_path, content):
    print(f"Writting {s3_path}")
    s3b = boto3.resource("s3").Bucket("musingsole")
    content_bytes = BytesIO(content.encode("utf-8"))
    s3b.upload_fileobj(content_bytes, s3_path)


def write_json(s3_path, content):
    print(f"Writing JSON at {s3_path}")
    write_s3(s3_path, json.dumps(content))
