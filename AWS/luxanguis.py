import json
import base64

from util import read_file
from s3 import retrieve_json, write_json


configuration_path = "lux_anguis_configuration"
queue_path = "lux_anguis_queue"


def retrieve_configuration():
    return retrieve_json(configuration_path)


def get_configuration(event=None):
    configuration = retrieve_configuration()
    return 200, configuration


def write_configuration(config):
    print("Writing configuration")
    write_json(configuration_path, config)


def configure_form(event=None):
    configuration = retrieve_configuration()
    configuration_page = read_file("html/configure_template.html")
    configuration['rows'] = configuration['layout'][0]
    configuration['columns'] = configuration['layout'][1]

    for key in ["num_leds", "rows", "columns"]:
        configuration_page = configuration_page.replace(
            f"{{{key}}}", str(configuration[key]))

    return 200, configuration_page


def post_configuration(event):
    if event.get('isBase64Encoded', ''):
        config_str = event['body'].encode('utf-8')
        configuration = json.loads(base64.decodebytes(config_str).decode())
    else:
        configuration = json.loads(event['body'])
    print(f"Writing configuration: {configuration}")
    write_configuration(configuration)
    return 200
