def read_file(path):
    with open(path, "r") as f:
        contents = f.read()
    return contents


def format_html_template(template, **config):
    for key, value in config.items():
        template = template.replace(f"{{{{{key}}}}}", str(value))
    return template
