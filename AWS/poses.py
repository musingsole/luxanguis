import json
from urllib.parse import unquote as url_decode
from s3 import list_s3, retrieve_json, write_json


class Pose:
    def __init__(self, pose_name, led_states=None, layout='neopixel', brightness=100):
        self.pose_name = pose_name
        self.layout = layout
        self.led_states = led_states or []
        self.brightness = brightness

    @property
    def dict(self):
        return {"pose_name": self.pose_name,
                "layout": self.layout,
                "led_states": self.led_states,
                "brightness": self.brightness}


def retrieve_pose(pose_name):
    print(f"Retrieving {pose_name}")
    s3_path = f"poses/{pose_name}"
    return Pose(**retrieve_json(s3_path))


def get_pose(event=None):
    try:
        pose_name = url_decode(event['pathParameters']['pose_name'])
    except Exception:
        pose_name = "current"

    pose = retrieve_pose(pose_name)
    return 200, json.dumps(pose.dict)


def list_poses():
    poses = list_s3("poses/")
    return [pose.key[len("poses/"):] for pose in poses]


def get_poses(event):
    return 200, list_poses()


def write_pose(pose):
    print(f"Writing pose {pose.pose_name}")
    s3_path = f"poses/{pose.pose_name}"
    write_json(s3_path, pose.dict)


def post_pose(event):
    print("Defining pose")
    pose = json.loads(event['body'])
    pose['pose_name'] = pose.pop('pose')
    pose = Pose(**pose)
    write_pose(pose)
    return 204
