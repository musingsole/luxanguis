from util import read_file, format_html_template
from layouts import list_layouts
from poses import list_poses
from animations import list_animations


def datalistize(data):
    return "\n" + "\n".join([f'<option value="{datum}">' for datum in data])


def build_layouts_datalist():
    return datalistize(list_layouts())


def build_poses_datalist():
    return datalistize(list_poses())


def build_animations_datalist():
    return datalistize(list_animations())


def layout_builder(event):
    page = read_file("html/layout_builder_template.html")
    config = {"layouts_list": build_layouts_datalist(),
              "led_nums": datalistize(range(100))}
    page = format_html_template(page, **config)
    return 200, page


def pose_builder(event):
    page = read_file("html/pose_builder_template.html")
    poses_list = build_poses_datalist()
    layouts_list = build_layouts_datalist()
    config = {"poses_list": poses_list,
              "layouts_list": layouts_list}
    for key, value in config.items():
        page = page.replace(f"{{{{{key}}}}}", str(value))
    return 200, format_html_template(page, **config)


def animation_builder(event):
    # TODO Fill datalist with all animations
    # TODO Read table on submit and create new animation
    page_template = read_file("html/animation_builder_template.html")

    page = format_html_template(
        page_template,
        animations_list=build_animations_datalist(),
        poses_list=build_poses_datalist())
    return 200, page


def configuration_builder(event):
    page = read_file("html/configuration_editor_template.html")
    config = {"animations_list": build_animations_datalist()}
    return 200, format_html_template(page, **config)


def unified(event):
    page = read_file("html/unified_template.html")
    return 200, format_html_template(page)
