import json
from urllib.parse import unquote as url_decode
from s3 import write_json, retrieve_json, list_s3


class Layout:
    def __init__(self, layout_name, definition):
        self.layout_name = layout_name
        self.definition = definition

    @property
    def dict(self):
        return {"layout_name": self.layout_name,
                "definition": self.definition}


def retrieve_layout(layout_name):
    print(f"Retrieving {layout_name}")
    s3_path = f"layouts/{layout_name}"
    return Layout(**retrieve_json(s3_path))


def get_layout(event=None):
    try:
        layout_name = url_decode(event['pathParameters']['layout_name'])
    except Exception:
        layout_name = "current"

    layout = retrieve_layout(layout_name)
    return 200, json.dumps(layout.dict)


def list_layouts():
    layouts = list_s3("layouts/")
    return [layout.key[len("layouts/"):] for layout in layouts]


def get_layouts(event):
    return 200, list_layouts()


def write_layout(layout):
    print(f"Writing layout {layout.layout_name}")
    s3_path = f"layouts/{layout.layout_name}"
    write_json(s3_path, layout.dict)


def post_layout(event):
    print("Defining layout")
    layout = json.loads(event['body'])
    layout['layout_name'] = layout.pop('layout')
    layout = Layout(**layout)
    write_layout(layout)
    return 204
