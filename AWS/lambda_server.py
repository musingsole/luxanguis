from LambdaPage import LambdaPage


from luxanguis import get_configuration, post_configuration
from layouts import get_layouts, get_layout, post_layout
from poses import get_poses, get_pose, post_pose
from animations import get_animations, get_animation, post_animation
from builders import configuration_builder, layout_builder, pose_builder, animation_builder, unified


def build_page():
    page = LambdaPage()
    page.add_endpoint(method='get', path='/configuration', func=get_configuration)
    page.add_endpoint(method='post', path='/configuration', func=post_configuration)
    page.add_endpoint(method='get', path='/configuration_builder', func=configuration_builder, content_type="text/html")
    page.add_endpoint(method='get', path='/layouts', func=get_layouts)
    page.add_endpoint(method='get', path='/layouts/{layout_name}', func=get_layout)
    page.add_endpoint(method='post', path='/layouts/{layout_name}', func=post_layout)
    page.add_endpoint(method='get', path='/layout_builder', func=layout_builder, content_type="text/html")
    page.add_endpoint(method='get', path='/poses', func=get_poses)
    page.add_endpoint(method='get', path='/poses/{pose_name}', func=get_pose)
    page.add_endpoint(method='post', path='/poses/{pose_name}', func=post_pose)
    page.add_endpoint(method='get', path='/pose_builder', func=pose_builder, content_type="text/html")
    page.add_endpoint(method='get', path='/animations', func=get_animations)
    page.add_endpoint(method='get', path='/animations/{animation_name}', func=get_animation)
    page.add_endpoint(method='post', path='/animations/{animation_name}', func=post_animation)
    page.add_endpoint(method='get', path='/animation_builder', func=animation_builder, content_type="text/html")
    page.add_endpoint(method='get', path='/unified', func=unified, content_type="text/html")
    return page


def lambda_handler(event, context):
    print(f"Handling event: {event}")
    page = build_page()
    results = page.handle_request(event)
    print(results['statusCode'])
    return results


if __name__ == "__main__":
    page = build_page()
    page.start_local()
