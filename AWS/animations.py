import json
from urllib.parse import unquote as url_decode

from s3 import list_s3, retrieve_json, write_json


class AnimationPose:
    def __init__(self, pose_name, duration):
        self.pose_name = pose_name
        self.duration = duration

    @property
    def dict(self):
        return {"pose_name": self.pose_name, "duration": self.duration}


class Animation:
    def __init__(self, animation_name, poses):
        self.animation_name = animation_name
        self.poses = []
        for pose in poses:
            try:
                self.poses.append(AnimationPose(**pose))
            except Exception:
                raise Exception(f"{pose} in {animation_name} is misdefined")

    @property
    def dict(self):
        return {"animation_name": self.animation_name,
                "poses": [pose.dict for pose in self.poses]}


def retrieve_animation(animation_name):
    print(f"Retrieving {animation_name}")
    s3_path = f"animations/{animation_name}"
    return Animation(**retrieve_json(s3_path))


def get_animation(event):
    animation_name = url_decode(event['pathParameters']['animation_name'])
    animation = retrieve_animation(animation_name)
    return 200, json.dumps(animation.dict)


def list_animations():
    animations = list_s3("animations/")
    return [animation.key[len("animations/"):] for animation in animations]


def get_animations(event):
    return 200, json.dumps(list_animations())


def write_animation(animation):
    print(f"Writing animation {animation.animation_name}")
    s3_path = f"animations/{animation.animation_name}"
    write_json(s3_path, animation.dict)


def post_animation(event):
    print("Defining animation")
    animation = json.loads(event['body'])
    animation = Animation(**animation)
    write_animation(animation)
    return 204
